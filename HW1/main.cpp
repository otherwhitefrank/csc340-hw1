/*! \mainpage Programming Methodology: HW1
 *
 * \section intro_sec Introduction
 *
 * This is a simple program to run several problems from the book, chiefly:<br>
 *	Problem 11: Merging two files containing a list of inorder integers into an external file.<br>
 *	Problem 2: Read in a NxN matrix and determine if it is a magic matrix or not (all rows and diagonals
 *			   sum to the same number)<br>
 *	Problem 3: Reading in prose, counting and reorganizing the words in alphabetical order<br>
 *
 * \section install_sec Installation
 *	
 * This program should compile and run out of the box. Care must be taken when asked for file names
 * to make sure the file is in the working directory or give the full path to the file.
 * other then that just have fun!
 * 
 */


/* 
	AUTHOR: Frank Dye
	Student ID: 912927332
	Date: 9/3/13
	Copyright: Frank Dye, 2013
	Description: main: A driver class to execute homework functions 11, 2, 3,
		Problem 11: merging two files containing a list of inorder integers into an external file.
		Problem 2: read in a NxN matrix and determine if it is a magic matrix or not
		Problem 3: Reading in prose, counting and reorganizing the words in alphabetical order
	Compiler: MS Visual Studio 2012 
*/

#include <iostream>
#include <string>
#include <fstream>
#include <queue>
#include <list>
#include <stack>
#include <set>

using namespace std;

//Header Def's
void problem11(ifstream& inFile1, ifstream& inFile2, ofstream& outFile1);
bool problem2(list<int>& fileElements, int N);
void problem3();
string strUpper(string& inString);

int main()
{
	/** main: A driver class to execute homework functions 11, 2, 3,
	* @pre none
	* @post problems 11, 2, 3 are executed
	* @param none
	* @return 0
	*/ 

	string temp;
	string inFileName1, inFileName2, outFileName1;

	//Problem 11: Merge two files of inorder integers problem.

	//Create filestreams
	ifstream inFile1, inFile2;
	ofstream outFile1;

	//Open the files
	//Save the names of the files and open them
	bool problemOpeningFiles = false;

	//Get the names of our files to open
	cout << "Problem 11: Merge two files of inorder integers into a third file\n\n";
	cout << "Please enter name of first file: ";
	cin >> inFileName1;
	cout << "Please enter name of second file: ";
	cin >> inFileName2;
	cout << "Please enter name of output file: ";
	cin >> outFileName1;

	//Now open each file displaying an error if it fails to open.
	inFile1.open(inFileName1);
	inFile2.open(inFileName2);
	outFile1.open(outFileName1);

	//Check that the files were opened properly
	if (inFile1.fail())
	{
		cerr << "Problem opening first input file: " << inFileName1 << "\n";
		problemOpeningFiles = true;
	}

	if (inFile2.fail())
	{
		cerr << "Problem opening second input file: " << inFileName2 << "\n";
		problemOpeningFiles = true;
	}

	if (outFile1.fail())
	{
		cerr << "Problem opening output file: " << outFileName1 << "\n";
		problemOpeningFiles = true;
	}

	if (problemOpeningFiles)
	{
		cerr << "ERROR: Unable to open one of the files, Aborting merge" << "\n";
	}
	else
	{
		problem11(inFile1, inFile2, outFile1);
	}

	//Close all file streams
	inFile1.close();
	inFile2.close();
	outFile1.close();

	//Problem 2: read in a NxN matrix and determine if it is a magic matrix or not

	list<int> fileElements;
	int userEnteredN, userEnteredElement;

	//MagicMatrix problem
	cout << "\nProblem 2: Read in a file containing a matrix and determine if it holds the 'magic' property\n\n";
	cout << "Please enter N for the matrix: ";
	cin >> userEnteredN;
        
	cout << "Please enter matrix elements seperated by spaces, <return> to end: ";
	
	do
	{
		//Put each element in the array in our fileElements list
		cin >> userEnteredElement;

		fileElements.push_back(userEnteredElement);
	} while (cin.peek() != '\n');


	if (userEnteredN != (int) sqrt(fileElements.size() + 1))
	{
		cerr << "ERROR: Number of elements does not match user entered N" << "\n";
	}
	else
	{
		//Display the matrix 

		//Iterate over the list printing a newline every arrayN elements
		list<int>::iterator it;

		cout << "\n\nDisplaying Matrix: \n";
		int itCount = 0;
		for (it = fileElements.begin(); it != fileElements.end(); ++it)
		{
			cout << *it << " ";

			if ((itCount + 1) % userEnteredN == 0)
				cout << "\n";

			itCount++;
		}

		cout << "\n";
		if (problem2(fileElements, userEnteredN))
			cout << "Matrix is a magic matrix!\n";
		else
			cout << "Matrix is not a magic matrix!\n";
	}

	cout << "\n\n";

	//Problem 3: Reading in prose, counting and reorganizing the words in alphabetical order
	cout << "Problem 3: \n\n";

	problem3();

	//End the program

	cout << "Finished program!";

	cin >> temp;

	return 0;
}

//Simple function to uppercase strings to A-Z
string strUpper(string& inString)
{
	/** strUpper: a simple function to convert a string to uppercase.
	* @pre inString must be a valid null delimitated string
	* @post inString is all uppercase
	* @param string& inString
	* @return inString
	*/ 


	string outString;
	for (int i = 0; i < inString.size(); i ++)
		outString.push_back(toupper(inString[i]));

	return outString;
}


void problem3()
{
	/** Reading in prose, counting and reorganizing the words in alphabetical order
	*	An easy way to solve this is to use a set/multi-set combo.
	*   The multi-set allows multiple instances of a key and can be used to query total word count
	*   A regular set eleminates duplicates and can be used to alphabetize the list of prose
	* @pre none
	* @post outputs a count of each word in the porse
	* @param none
	* @return none
	*/ 


	//Define a std::less<string> < symbol comparator, basicly a lexiconical checker
	
	std::set<string, std::less<string>> proseSet;
	std::multiset<string, std::less<string>> proseMultiSet;

	string userEnteredString;
        
	cout << "Please enter line of prose seperated by spaces: ";
	do
	{
		//Put each element in the array in our fileElements list
		cin >> userEnteredString;
		if (userEnteredString.size() > 8)
		{
			//Truncate to 8 characters
			userEnteredString[8] = 0;
		}

		userEnteredString = strUpper(userEnteredString);
		proseSet.insert(userEnteredString);
		proseMultiSet.insert(userEnteredString);
	} while (cin.peek() != '\n');

	//Display the word 

	std::multiset<string>::iterator multiIt;
	std::set<string>::iterator setIt;

	cout << "Word Count: \n";
	for (setIt = proseSet.begin(); setIt != proseSet.end(); ++ setIt)
	{
		cout << *setIt << ": " << proseMultiSet.count(*setIt) << "\n";
	}

	cout << '\n';
}

void problem11(ifstream& inFile1, ifstream& inFile2, ofstream& outFile1)
{
	/**Solution to problem 11, merging two files containing a list of inorder integers
	* @pre inFile1 && infile2 != NULL. outFile1 is opened and ready to be written to
	* @post outFile1 is opened and written
	* @param 
	*	ifstream& inFile1 : an opened ifstream file source
	*	ifstream& inFile2 : an opened ifstream file source
	*	ofstream& outfile1 : an opened ofstream file source
	* @return none
	*/ 

	//First define a temporary integer and two queue's to store our inorder list of integers
	int tempNum = 0;
	queue<int> queue1, queue2;

	//Load each queue from the files

	while (!inFile1.eof())
	{
		inFile1 >> tempNum;
		queue1.push(tempNum);
	}

	while (!inFile2.eof())
	{
		inFile2 >> tempNum;
		queue2.push(tempNum);
	}

	//Both queue's are loaded, the front == the smallest elements

	while (!queue1.empty() || !queue2.empty())
	{
		//If there are still numbers in our queue's

		if (!queue1.empty() && !queue2.empty())
		{
			//If neither queue is empty we take the lesser of the front of the queue's
			if (queue1.front() <= queue2.front())
			{
				outFile1 << queue1.front();
				queue1.pop();
			}
			else
			{
				outFile1 << queue2.front();
				queue2.pop();
			}
		}
		else if (queue1.empty())
		{
			//If the first queue is empty then take from the second queue
			outFile1 << queue2.front();
			queue2.pop();
		}
		else if (queue2.empty())
		{
			//If the second queue is empty then take from the first queue
			outFile1 << queue1.front();
			queue1.pop();
		}
		
		//Add space to seperate numbers
		outFile1 << "\n";
	}

	//Once both queue's are empty we are done.
}

bool problem2(list<int>& fileElements, int N)
{
	/**merging two files containing a list of inorder integers into an external file.
	* @pre fileElements contains a list of matrix entries, N equals the N dimension of the matrix
	* @post calculates if a matrix is magic or not
	* @param 
	*	list<int>& fileElements : A list of each entry in the matrix
	*	int N : The NxN dimensionality of the matrix
	* @return returns true if the matrix is a magic matrix, false if it is not.
	*/ 
	int sum = 0;

	//create a dynamic 2-dimensional array
	int **matrixArray = new int *[N];

	for (int i = 0; i < N; i++)
		matrixArray[i] = new int [N];

	//Copy from our list of integers to our matrix in the proper position
	list<int>::iterator it;
	it = fileElements.begin();
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			matrixArray[i][j] = *it;
			it++;
		}
	}
	//There will be a total for each sum of row and column plus the two diagonals
	
	//Convenient stack to store all the sum's
	stack<int> answerStack;

	for (int i = 0; i < N; i++)
	{
		//Sum the row
		for (int j = 0; j < N; j++)
		{
			sum += matrixArray[i][j];
		}

		answerStack.push(sum);
		sum = 0;
		
		//Sum the column
		for (int j = 0; j < N; j++)
		{
			sum += matrixArray[j][i];
		}

		answerStack.push(sum);

		sum = 0;

	}

	//Sum the first diagonal 
	sum = 0;
	for (int i = 0; i < N; i++)
	{
		sum += matrixArray[i][i];
	}

	answerStack.push(sum);
	sum = 0;


	//Sum the second diagonal
	for (int i = 0; i < N; i++)
	{
		sum += matrixArray[(N-1) - i][i];
	}

	answerStack.push(sum);
	sum = 0;

	//Check to make sure each entry in answerArray equals another

	bool matrixIsMagic = true;
	int first = answerStack.top();
	answerStack.pop();
	int second = answerStack.top();
	answerStack.pop();

	while (!answerStack.empty() && matrixIsMagic)
	{
		if (first != second)
			matrixIsMagic = false;

		first = second;
		second = answerStack.top();
		answerStack.pop();
	}

	return matrixIsMagic;
}